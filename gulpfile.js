var gulp    	= require( 'gulp' ),
	browserSync = require( 'browser-sync' ).create(),
	concat      = require( 'gulp-concat' ),
	copy        = require( 'gulp-copy' ),
	cssmin      = require( 'gulp-cssmin' ),
	imagemin    = require( 'gulp-imagemin' ),
	rename      = require( 'gulp-rename' ),
	sass        = require( 'gulp-sass' ),
	uglify      = require( 'gulp-uglify' ),
	pngquant    = require( 'imagemin-pngquant' );

gulp.task( 'styles', function(){
	gulp.src( ['assets/css/*.css', '!assets/css/*.min.css'], { base: '.' } )
		.pipe( cssmin() )
		.pipe( rename( { suffix: '.min' } ) )
		.pipe( gulp.dest( './' ) )
});

gulp.task( 'scripts', function(){
	gulp.src( ['assets/js/*.js', 'assets/!js/*.min.js'] )
		.pipe( uglify() )
		.pipe( rename( { suffix: '.min' } ) )
		.pipe( gulp.dest( 'js/' ) )
});

gulp.task( 'imagemin', function(){
	gulp.src( 'images/**' )
		.pipe(
			imagemin({
				progressive: true,
				interlaced: true,
				svgoPlugins: [
				    { removeViewBox: false },
				    { cleanupIDs: false }
				],
				use: [pngquant()]
			})
		)
		.pipe( gulp.dest( 'images' ) )
});

gulp.task( 'sass', function() {
 	gulp.src( 'scss/**/*.scss' )
	    .pipe( sass( { outputStyle: 'expanded' }	).on( 'error', sass.logError ) )
	    .pipe( gulp.dest( './' ) )
	    .pipe( browserSync.stream() );
});

gulp.task( 'serve', function() {

    browserSync.init({
        proxy: "http://localhost/theme-docs",
        notify: false,
        ghostMode: {
        	forms: false,
        }
        // Tunnel the Browsersync server through a random Public URL
        // -> http://randomstring23232.localtunnel.me
        //tunnel: true,
        // Attempt to use the URL "http://my-private-site.localtunnel.me"
        //tunnel: "pextestera23",
    });

    gulp.watch("scss/*.scss", ['sass', 'styles']);

    gulp.watch('style.css', function() {
      gulp.src('style.css')
        .pipe(browserSync.stream());
    });

    gulp.watch("**/*.css").on('change', browserSync.reload);
    gulp.watch("**/*.js").on('change', browserSync.reload);
    gulp.watch("**/*.html").on('change', browserSync.reload);
});