$(document).ready(function() {

    /*===================================
    =            Active link            =
    ===================================*/
    var hash = window.location.hash;
    if (hash.length) {
        $('.menu a[href="' + hash + '"]').parent('li').addClass('active');
    }

    /*======================================
    =            Section object            =
    ======================================*/
    var sections = {};

    $('.section').each(function(i) {
        if ($(this).hasClass('first')) {
            return;
        }
        var self = $(this);
        var top = self.offset().top;
        var bottom = top + self.outerHeight(true);
        sections['#'+self.attr('id')] = {
            topOffset: top - 100,
            bottomOffset: bottom - 100,
            subSections: function() {
                if (!$('.subsection', self).length) {
                    return false;
                }
                data = {};
                $('.subsection', self).each(function(){
                    var id = '#'+$(this).attr('id');
                    var top = $(id).offset().top - 70;
                    var bottom = top + $(id).outerHeight(true) - 70;

                    data[id] = {
                        topOffset: top,
                        bottomOffset: bottom,
                    };
                });
                return data;
            },
        };
    });

    /*====================================
    =            Scroll event            =
    ====================================*/
    $(window).scroll(function(e) {

        if ($(window).scrollTop() + $(window).height() == $(document).height()) {
            $('.menu li').removeClass('active');
            $('.menu li:last-child').addClass('active');
            return;
        }

        if ($('html,body').is(':animated')) {
            return;
        }

        var topPos = $(window).scrollTop();

        for (var section in sections) {
            if (topPos > sections[section].topOffset && topPos <= sections[section].bottomOffset) {
                $('.menu a[href="' + section + '"]').parent('li').addClass('active');
                $('.submenu li').removeClass('active');
                var subSections = sections[section].subSections();
                if ( subSections ) {
                    for( var subSection in subSections ) {
                        if (topPos > subSections[subSection].topOffset && topPos <= subSections[subSection].bottomOffset) {
                            $('.submenu li').removeClass('active');
                            $('.submenu a[href="'+subSection+'"]').parent('li').addClass('active');
                        }
                    }
                }
            } else {
                $('.menu a[href="' + section + '"]').parent('li').removeClass('active');
            }
        }
    });

    /*=====================================
    =            Link Handling            =
    =====================================*/

    $('.menu a, .section-link').click(function(e) {

        e.preventDefault();

        var id = $(this).attr('href');
        var top = $(id).offset().top - 25;

        $('.menu a[href="' + id + '"]').parent('li').addClass('active');

        $('html,body').animate({
            scrollTop: top
        }, 500);

        window.location.hash = $(this).attr('href');
    });

    $('.menu i').click(function(){
        $('.menu li.active').removeClass('active');
        $(this).parent().toggleClass('active');
    });

    /*======================================
    =            Sticky Sidebar            =
    ======================================*/

    $('.content, .sidebar').theiaStickySidebar({
        additionalMarginTop: 30
    });

    /*===============================
    =            Fitvids            =
    ===============================*/

    $('.content').fitVids();

});
